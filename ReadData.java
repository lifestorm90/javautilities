package data;

import java.util.Scanner;

public class ReadData {

	public static String readString() {
		String word = null;
		try (Scanner sc = new Scanner(System.in)) {
			word = sc.nextLine();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return word;
	}

	public static String readString(String msg) {
		String word = null;
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println(msg);
			word = readString();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return word;
	}

	public static byte readByte() {
		byte num = 0;
		try (Scanner sc = new Scanner(System.in)) {
			num = sc.nextByte();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;
	}

	public static byte readByte(String msg) {
		byte num = 0;
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println(msg);
			num = readByte();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;
	}

	public static int readInt() {
		int num = 0;
		try (Scanner sc = new Scanner(System.in)) {
			num = sc.nextInt();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;
	}

	public static int readInt(String msg) {
		int num = 0;
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println(msg);
			num = readInt();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;
	}

	public static long readLong() {
		long num = 0;
		try (Scanner sc = new Scanner(System.in)) {
			num = sc.nextLong();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;

	}

	public static long readLong(String msg) {
		long num = 0;
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println(msg);
			num = readLong();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;

	}

	public static float readFloat() {
		float num = 0f;
		try (Scanner sc = new Scanner(System.in)) {
			num = sc.nextFloat();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;
	}

	public static float readFloat(String msg) {
		float num = 0f;
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println(msg);
			num = readLong();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;
	}

	public static double readDouble() {
		double num = 0d;
		try (Scanner sc = new Scanner(System.in))
		{
			num = sc.nextDouble();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;
	}

	public static double readDouble(String msg) {
		double num = 0d;
		try (Scanner sc = new Scanner(System.in))
		{
			System.out.println(msg);
			num = readDouble();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return num;
	}
}
